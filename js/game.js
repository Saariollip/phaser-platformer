var game = new Phaser.Game(800, 554, Phaser.AUTO, 'game', {preload: preload, create: create, update: update});


function preload() {

  /* Loading Map */
  game.load.tilemap('level3', 'assets/level2.json', null, Phaser.Tilemap.TILED_JSON);
  game.load.image('spritesheet', 'assets/spritesheet.png');

  /*Loading background */
  game.load.image('background', 'assets/background.png');

  /* Loading character sprites */
  game.load.spritesheet('dude', 'assets/dude.png', 32, 48);
  game.load.spritesheet('droid', 'assets/droid.png', 32, 32);

  /* Loading other sprites */
  game.load.image('starSmall', 'assets/star.png');
  game.load.image('starBig', 'assets/star2.png');
  game.load.spritesheet('blocks', 'assets/blocks.png', 21, 21);
  game.load.image('hitBlock', 'assets/hitBlock.png', 21, 21);


}

/* Initializing variables */
var map;
var layer;
var block;
var bg;

var cursors;
var facing = 'right';
var jumpTimer = 0;
var jumpButton;
var runButton;

var bullets;
var bulletTime = 0;
var fireButton;

function create() {

  game.physics.startSystem(Phaser.Physics.ARCADE);

  game.stage.backgroundColor = '#5E81A2';

  bg = game.add.tileSprite(0, 0, 800, 554, 'background');
  bg.fixedToCamera = true;


  map = game.add.tilemap('level3');
  map.addTilesetImage('spritesheet');
  map.setCollisionByExclusion([ 13, 14, 15, 16, 46, 47, 48, 49, 50, 51 ]);

  layer = map.createLayer('Tile Layer 1');
  //  Un-comment this on to see the collision tiles
  // layer.debug = true;

  layer.resizeWorld();

  game.physics.arcade.gravity.y = 250;


  /* Player */
  player = game.add.sprite(32, 460, 'dude');
  game.physics.enable(player, Phaser.Physics.ARCADE);
//  player.body.bounce.y = 0.2;
//  player.body.collideWorldBounds = true;

  player.body.setSize(20, 32, 5, 16);

  player.animations.add('left', [0, 1, 2, 3], 10, true);
  player.animations.add('turn', [4], 20, true);
  player.animations.add('right', [5, 6, 7, 8], 10, true);

  game.camera.follow(player);

  cursors = game.input.keyboard.createCursorKeys();
  jumpButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
  runButton = game.input.keyboard.addKey(Phaser.Keyboard.SHIFT);


  /* Block */
  block = game.add.sprite(100,450, 'blocks');
  game.physics.enable(block, Phaser.Physics.ARCADE);
  block.body.allowGravity = false;
  block.body.immovable = true;
  //block.setCollisionByExclusion([]);
  //block.body.setSiz


  /* Shooting */
  bullets = game.add.group();
  bullets.enableBody = true;
  bullets.physicsBodyType = Phaser.pPhysics.Arcade;
  bullets.createMultiple(30, ' bullet');
  bullets.setAll('achor.x', 0.5);
  bullets.setAll('achor.x', 0.5);
  bullets.setAll('outOfBoundsKill', true);
  bullets.setAll('checkWorldBounds', true);

  fireButton = game.input.activePointer.leftButton.isDown;
}

function update(){


    game.physics.arcade.collide(player, layer);
    //game.physics.arcade.collide(player, block);
    game.physics.arcade.collide(player, block, onBlockHit);

    player.body.velocity.x = 0;

    if (cursors.left.isDown)
    {
        player.body.velocity.x = -100;

        if (facing != 'left')
        {
            player.animations.play('left');
            facing = 'left';
        }
    }
    else if (cursors.right.isDown)
    {
        player.body.velocity.x = 100;

        if (facing != 'right')
        {
            player.animations.play('right');
            facing = 'right';
        }
    }
    else
    {
        if (facing != 'idle')
        {
            player.animations.stop();

            if (facing == 'left')
            {
                player.frame = 0;
            }
            else
            {
                player.frame = 5;
            }

            facing = 'idle';
        }
    }

    if (jumpButton.isDown && player.body.onFloor() && game.time.now > jumpTimer)
    {
        player.body.velocity.y = -250;
        jumpTimer = game.time.now + 750;
    }

    if (runButton.isDown && player.body.onFloor() && game.time.now > jumpTimer)
    {
      if (cursors.left.isDown)
      {
          player.body.velocity.x = -150;

          if (facing != 'left')
          {
              player.animations.play('left');
              facing = 'left';
          }
      }

      /* Not working
      else if (cursors.right.isDown)
      {
          player.body.velocity.x = 150;

          if (facing != 'right')
          {
              player.animations.play('right');
              facing = 'right';
          }
      }
      */

    }
    /* FIx
    else if (runButton.isDown && jumpButton.isDown && player.body.onFloor() && game.time.now > jumpTimer)
    {
      player.body.velocity.y = -150;
      player.body.velocity.x = 150;
      jumpTimer = game.time.now + 750;
    }
    */
}
function render () {

     game.debug.text(game.time.physicsElapsed, 32, 32);
     game.debug.body(player);
     game.debug.bodyInfo(player, 16, 24);

}

/* Own methods */
function onBlockHit(player, object){
  //game.add.sprite(object.body.x,object.body.y-20, 'hitBlock');
  if (player.deltaY < 0){
      object.loadTexture('hitBlock');
  }

}
